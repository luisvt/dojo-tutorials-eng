define([
    'dojo/_base/declare',
    'app/views/First',
    'dijit/layout/BorderContainer',
    'dijit/layout/ContentPane',
    'dijit/layout/AccordionContainer',
    'dijit/layout/TabContainer',
    'app/views/ValidationForm',
    'dojo/domReady!'
], function(
        declare,
        First,
        BorderContainer,
        ContentPane,
        AccordionContainer,
        TabContainer,
        ValidationForm
        ) {
    return declare([BorderContainer], {
        children: [
            new ContentPane({region: 'top', title: 'top', content: 'Top'}),
            new AccordionContainer({
                region: 'left', title: 'Left', content: 'Left',
                children: [
                    new ContentPane({title: 'Top', content: 'Top'}),
                    new ContentPane({title: 'Middle', content: 'Middle'}),
                    new ContentPane({title: 'Bottom', content: 'Bottom'})
                ]
            }),
            new TabContainer({
                region: 'center', title: 'center', content: 'Center',
                children: [
                    new ContentPane({
                        title: 'Left',
                        children: [
                            new First({title: 'Some Widget'})
                        ]
                    }),
                    new ContentPane({
                        title: 'Middle',
                        children: [
                            new ValidationForm()
                        ]
                    }),
                    new ContentPane({title: 'Right', content: 'Right'})
                ]
            }),
            new ContentPane({region: 'right', title: 'right', content: 'Right'}),
            new ContentPane({region: 'bottom', title: 'bottom', content: 'Bottom'})
        ]
    });
});


