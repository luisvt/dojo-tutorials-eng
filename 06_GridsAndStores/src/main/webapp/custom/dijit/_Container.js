define([
    'dojo/_base/declare',
    location.href + 'webjars/dojo-src/1.9.3/dijit/_Container.js'
], function(declare, _Container) {

    return declare([_Container], {
        children: [],
        
        postCreate: function(params, srcNodeRef) {
            this.inherited(arguments);
            
            for(var i in this.children) {
                this.addChild(this.children[i]);
            }
        }
    });
});