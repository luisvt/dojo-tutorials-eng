define([
    'dojo/_base/declare',
    'app/views/First',
    'dijit/layout/BorderContainer',
    'dijit/layout/ContentPane',
    'dijit/layout/AccordionContainer',
    'dijit/layout/TabContainer',
    'app/views/ValidationForm'
], function(
    declare,
    First,
    BorderContainer,
    ContentPane,
    AccordionContainer,
    TabContainer,
    ValidationForm
        ) {
    return declare([BorderContainer], {
        postCreate: function() {
            this.inherited(arguments);
            this.addChild(new ContentPane({region: 'top', title: 'top', content: 'Top'}));
            var ac = new AccordionContainer({region: 'left', title: 'left', content: 'Left'});
                ac.addChild(new ContentPane({title: 'Top', content: 'Top'}));
                ac.addChild(new ContentPane({title: 'Middle', content: 'Middle'}));
                ac.addChild(new ContentPane({title: 'Bottom', content: 'Bottom'}));
            this.addChild(ac);
            var tc = new TabContainer({region: 'center', title: 'center', content: 'Center'});
                var lcp = new ContentPane({title: 'Left'});
                    lcp.addChild(new First({title: 'Some Widget'}));
                tc.addChild(lcp);
                
                var ccp = new ContentPane({title: 'Middle'});
                    ccp.addChild(new ValidationForm());
                tc.addChild(ccp);
                tc.addChild(new ContentPane({title: 'Right', content: 'Right'}));
            this.addChild(tc);
            this.addChild(new ContentPane({region: 'right', title: 'right', content: 'Right'}));
            this.addChild(new ContentPane({region: 'bottom', title: 'bottom', content: 'Bottom'}));
        }
    })
});


