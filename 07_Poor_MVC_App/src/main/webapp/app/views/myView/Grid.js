define([
    'dojo/_base/declare',
    'gridx/Grid',
    'gridx/modules/select/Row',
    'app/stores/myStore',
], function(declare, Grid, SelectRow, myStore) {
    return declare([Grid], {
        store: myStore,
        selectRowTriggerOnCell: true,
        modules: [
            SelectRow
        ],
        structure: [
            {field: 'id', name: 'Identity'},
            {field: 'title', name: 'Title'},
            {field: 'artist', name: 'Arstist'}
        ]
    });
});