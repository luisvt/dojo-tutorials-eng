define([
    'dojo/_base/declare',
    'extended/dijit/form/Form',
    'dijit/form/TextBox',
    'dijit/form/Button',
    'dojox/layout/TableContainer',
    'dijit/form/NumberTextBox'
], function(declare, Form, TextBox, Button, TableContainer, NumberTextBox) {
    return declare([Form], {
        constructor: function() {
            this.children = [
                new TableContainer({
                    children: [
                        new NumberTextBox({label: 'id', name: 'id'}),
                        new TextBox({label: 'title', name: 'title'}),
                        new TextBox({label: 'artist', name: 'artist'})
                    ]
                }),
                new Button({
                    label: 'Save',
//                    onClick: function(event) {
//                        var value = this.get('value');
//
//                        myStore.put(value);
//                    }
                })
            ];
        }
    });
});