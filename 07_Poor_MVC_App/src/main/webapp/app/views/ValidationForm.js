define([
    'dojo/_base/declare',
    "dijit/form/Form",
    'dojox/layout/TableContainer',
    'dijit/form/ValidationTextBox',
    
    'dojox/validate/web'
], function(
    declare,
    Form,
    TableContainer,
    ValidationTextBox
    ) {
    return declare([Form], {
        postCreate: function() {
            var tc = new TableContainer({cols: 1});
                tc.addChild(new ValidationTextBox({required: true, title: 'First Name', name: 'firstName'}));
                tc.addChild(new ValidationTextBox({required: true, title: 'Last Name', name: 'lastName'}));
                tc.addChild(new ValidationTextBox({
                    required: true, title: 'Email', name: 'email',
                    validator: dojox.validate.isEmailAddress,
                    invalidMessage: 'This is not a valid email!'
                }));
                tc.addChild(new ValidationTextBox({
                    required: true, title: 'Age', name: 'age',
                    validator: dojox.validate.isInRange,
                    constraints: {min: 18, max: 100},
                    invalidMessage: 'This is not within the range of 18 to 100!'
                }));
            this.domNode.appendChild(tc.domNode);
        }
    });
});