require([
    'dojo/query',
    'dojo/ready',
    'dojo/on',
    'app/stores/myStore'
], function(query, ready, on, myStore) {
    ready(function() {
        var button = query('#myViewForm .dijitButton'),
            myViewForm = dijit.byId('myViewForm'),
            myViewGrid = dijit.byId('myViewGrid');

        on(button, 'click', function(event) {
            var value = myViewForm.get('value');
            value.id = parseInt(value.id);
            myStore.put(value);
        });
        
        myViewGrid.onRowClick = function(event) {
            myViewForm.set('value', myStore.get(event.rowId));
        };
    });
});